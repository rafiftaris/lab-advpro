package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Filling {
    Food food;

    public Cucumber(Food food) {
        //TODO Implement
        this.food = food;
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return this.food.getDescription() + ", adding cucumber";
    }

    @Override
    public double cost() {
        //TODO Implements
        return this.food.cost() + 0.4;
    }
}
